# README #

KICad Library

### What is this repository for? ###

This repository contains KiCAD parts and footprints for designign circuit boards

### How do I get set up? ###

Place this repository in a folder next to other RobotLogic repositories
Any KiCAD PCB designs will have a relative library path that finds these libraries
When creating a new KiCAD PCB:
	- In the schematic editor, use Preferences->ComponentLibraries to point to this library
	- In the PCB editor, use Preferences->Footprint Library Manager
	- Use relative paths

### Contribution guidelines ###

Just be careful when editing footprints or parts to ensure you don't break any existing designs

