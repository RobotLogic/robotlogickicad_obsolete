#
# Example python script to generate a BOM from a KiCad generic netlist
#
# Example: Sorted and Grouped CSV BOM
#

"""
    @package
    Generate a Tab delimited list (csv file type).
    Components are sorted by ref and grouped by value with same footprint
    Fields are (if exist)
    'Ref', 'Qnty', 'MFG Name', 'MFG Part Num', 'Value', 'Cmp name', 'Footprint', 'Description'

    Command line:
    python "pathToFile/bom_csv_grouped_by_value_with_fp.py" "%I" "%O.csv"
"""

# Import the KiCad python helper module and the csv formatter
import kicad_netlist_reader
import csv
import sys
import re


def natural_sort(l): 
    convert = lambda text: int(text) if text.isdigit() else text.lower() 
    alphanum_key = lambda key: [ convert(c) for c in re.split('([0-9]+)', key) ] 
    return sorted(l, key = alphanum_key)
	

# Generate an instance of a generic netlist, and load the netlist tree from
# the command line option. If the file doesn't exist, execution will stop
net = kicad_netlist_reader.netlist(sys.argv[1])

# Open a file to write to, if the file cannot be opened output to stdout
# instead
filename = sys.argv[2]
if not filename.lower().endswith('.csv'):
	filename = filename + '.csv'
	
	
try:
    f = open(filename, 'w')
except IOError:
    e = "Can't open output file for writing: " + filename
    print(__file__, ":", e, sys.stderr)
    f = sys.stdout

# Create a new csv writer object to use as the output formatter
out = csv.writer(f, lineterminator='\n', delimiter=',', quotechar='\"', quoting=csv.QUOTE_ALL)


# Get all of the components in groups of matching parts + values
# (see ky_generic_netlist_reader.py)
grouped = net.groupComponents()


# Output a set of rows for a header providing general information
out.writerow(['Source:', net.getSource()])
out.writerow(['Date:', net.getDate()])
out.writerow(['Tool:', net.getTool()])
out.writerow(['Generator:', sys.argv[0]])
out.writerow(['Component Count:', len(net.components)])
out.writerow(['Component Types:', len(grouped)])
out.writerow(['Ref', 'Qnty', 'MFG Name', 'MFG Part Num', 'Value', 'Cmp name', 'Footprint', 'Description'])

# Output all of the component information
for group in grouped:

    # Make a list of the refs for each component in the group and keep a reference
    # to the component so that the other data can be filled in once per group
	refs = []
	for component in group:
		refs.append(component.getRef())
		c = component
	
	# sort the refs so we can display them nicely
	#refs.sort(natural_sort_key)
	refs = natural_sort(refs)
	
	# create a string for the entire set of refs, don't use commas since they mess with .csv format!
	refstring = ""
	for ref in refs:
		refstring += ref + " "


    # Fill in the component groups common data
	out.writerow([refstring, len(group), c.getField("MFG Name"), c.getField("MFG Part Num"), c.getValue(), c.getPartName(), c.getFootprint(),c.getDescription()])


